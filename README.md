# Pasos para arrancar el proyecto

```sh
npm install
npm run dev
```

### Decisiones tomadas

- Realizé la tarea con Vue.js y utilizé Bootstrap CSS para los estilos.
- Creé un Array de objetos iniciales con 3 salas.
- Para guardar los datos he decidido utilizar Local Storage, ya que en la prueba no se mencionaba sobre alguna tecnología específica.
- P.S. No me concentré tanto en el diseño como en la funcionalidad, por lo que pudiera haberlo diseñado un poco más bonito :)


### Dificultades encontradas

- Encontré dificultad en listar salas de una planta seleccionada, o sea crear un select que mostraría salas diferentes en cada planta. Y también supongo que es lo que dificulta el buen funcionamiento de Bootstrap CSS para el diseño responsive.
